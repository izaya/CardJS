/**
 * Created by Francis Yang on 6/21/17.
 */
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Card Game');
var scoreText;
var myHand;
var cardDeck;
var drawSprite;
var score = 0;

var GameState = {
    // load image, assets and cache them
    preload: function () {
        this.load.image ('cardback', 'img/cardback.png');
        this.load.image ('tableBG', 'img/tableBG.png');
        this.load.image('1s', 'img/cards/ace_of_spades.png');
        this.load.image('2s', 'img/cards/2_of_spades.png');
        this.load.image('3s', 'img/cards/3_of_spades.png');
        this.load.image('4s', 'img/cards/4_of_spades.png');
        this.load.image('5s', 'img/cards/5_of_spades.png');
        this.load.image('6s', 'img/cards/6_of_spades.png');
        this.load.image('7s', 'img/cards/7_of_spades.png');
        this.load.image('8s', 'img/cards/8_of_spades.png');
        this.load.image('9s', 'img/cards/9_of_spades.png');
        this.load.image('ts', 'img/cards/10_of_spades.png');
        this.load.image('js', 'img/cards/jack_of_spades.png');
        this.load.image('qs', 'img/cards/queen_of_spades.png');
        this.load.image('ks', 'img/cards/king_of_spades.png');
    },
    // add texts and position image and texts
    create: function () {
        // game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
        // game.scale.parentIsWindow = true;
        // game.scale.fullScreenScaleMode = Phaser.ScaleManager.RESIZE
        var socket = io.connect("http://localhost:3000");
        // TODO: IF FAIL
        socket.on("connection confirmed", function () {
            var cardArray = [];
            var player0status = -1;
            socket.emit("game init", cardArray);
            socket.emit("player 0 status", player0status);
        });

        socket.on('error', (error) => {
            console.log(error);
        });

        game.physics.startSystem(Phaser.Physics.ARCADE);
        // background
        game.add.sprite (0, 0, 'tableBG');
        // score text
        scoreText = game.add.text (16, 16, 'Score: 0', { fontSize: '32px', fill: '#fff' });

        // sprite of card draw deck
        drawSprite = game.add.sprite (game.width - 100, game.height / 2, 'cardback');
        drawSprite.width = 75;
        drawSprite.height = 105;
        drawSprite.anchor.setTo (0.5, 0.5);
        drawSprite.inputEnabled = true;
        drawSprite.events.onInputDown.add(drawCard, this);

        // card deck init
        cardDeck = new Game();
        // your hands
        myHand = new Player();

        socket.on('game initialized', function (cardArray) {
            cardDeck._card = cardArray;
            console.log(cardDeck.get());
        });

        socket.on('player 0 initialized', function (player0status) {
            myHand.setState(player0status);
            console.log(myHand.getState());
        });
    },

    // game loop
    update: function () {
        scoreText.text = 'Score: ' + score;
        game.debug.body(myHand);

        switch (myHand._state) {
            case 0:
                // 0: chilling, not my turn
                break;
            case 1:
                // 1: my turn to attack
                break;
            case 2:
                // 2: I win
                break;
            case -1:
                // -1: I lose
                break;
        }
    }
};

window.onload = function () {
    game.state.add ('GameState', GameState);
    game.state.start ('GameState');
};

/**
 * Player instance
 */
// state 0: online chilling, not my turn
// state 1: my turn to attack
// state 2: I win
// state -1: I lose
// state -2: offline
function Player () {
    if (false === (this instanceof Player)) {
        return new Player();
    }
    this._playerID;
    this._Cards = [];
    this._state = -2;
}

Player.prototype.constructor = Player;

Player.prototype.getCards = function () {
    return this._Cards;
};

Player.prototype.setState = function (status) {
    this._state = status;
};

Player.prototype.getState = function () {
    return this._state;
};

Player.prototype.insert = function (card) {
    this._Cards.push(card);
};

// number of cards
Player.prototype.getSize = function () {
    return this._Cards.length;
};

Player.prototype.pop = function () {
    return this._Cards.pop();
};

/**
 * Card instance
 */
// state 0: in the deck or hand
// state 1: chilling on the table
// state 2: turn to attack
// state -1: destroy
function Card (id, img, health, damage) {
    if (false === (this instanceof Card)) {
        return new Card();
    }
    Phaser.Sprite.call(this, game, game.width - 150, game.height / 2, img);
    this.id = id;
    this.img = img;
    this.health = health;
    this.damage = damage;
    this.state = 0;
    this.width = 50;
    this.height = 70;
}

Card.prototype = Object.create(Phaser.Sprite.prototype);
Card.prototype.constructor = Card;

Card.prototype.playCard = function (sprite, pointer) {
    switch (sprite.state) {
        case 0:
            // displayed from hand to table
            sprite.y -= 200;
            sprite.state = 1;
            break;
        case 1:
            break;
        case 2:
            // play card
            // tell server and reset to state 1
            break;
    }
};

/**
 * Game instance which can handle card deck
 */
function Game () {
    if (false === (this instanceof Game)) {
        return new Game ();
    }
    this._card = [];
}

Game.prototype.constructor = Game;

Game.prototype.get = function () {
    return this._card;
};

Game.prototype.pop = function () {
    return this._card.pop();
};

// create cards into the deck and shuffle
Game.prototype.init = function () {
    this._card.push (new Card (0, '1s', 1, 1));
    this._card.push (new Card (1, '2s', 1, 2));
    this._card.push (new Card (2, '3s', 1, 3));
    this._card.push (new Card (3, '4s', 1, 4));
    this._card.push (new Card (4, '5s', 1, 5));
    this._card.push (new Card (5, '6s', 1, 6));
    this._card.push (new Card (6, '7s', 1, 7));
    this._card.push (new Card (7, '8s', 1, 8));
    this._card.push (new Card (8, '9s', 1, 9));
    this._card.push (new Card (9, 'ts', 1, 10));
    this._card.push (new Card (10, 'js', 1, 11));
    this._card.push (new Card (11, 'qs', 1, 12));
    this._card.push (new Card (12, 'ks', 1, 13));
    this.shuffleArray(this._card);
};

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm or Fisher-Yates (aka Knuth) Shuffle.
 */
Game.prototype.shuffleArray = function (array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
};

function drawCard () {
    var card = cardDeck.pop();
    var tmp_card = card;
    game.physics.arcade.enable(card);

    // set card position
    card.x =  myHand.getSize() * (card.width + 10);
    card.y = game.height - card.height;

    //card.body.velocity.set((insertPosX - 650) * 0.5, (game.height - card.height - 300) * 0.5);
    //game.physics.arcade.moveToXY(card, insertPosX, game.height - card.height);
    //game.add.tween(card.body).to( { x: insertPosX, y: game.height - card.height }, 3000, Phaser.Easing.Linear.None, true);
    myHand.insert (card);
    game.add.existing (card);

    card.inputEnabled =  true;
    card.events.onInputDown.add(card.playCard, this);
}
