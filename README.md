# CardJS
This is an initial prototype of my online card game written in [PhaserJS](http://phaser.io/). Now the game client has been entirely written in Unity as I need more complicated UI to dynamically change the health and damage values of cards. So this repository is abandoned.  
The new project is at [https://gitlab.com/izaya/CardIO](https://gitlab.com/izaya/CardIO)  
You can still run this project using any web server and access index.html. The game file is in /js/main.js.
However the most recent commit requires server to display cards so you probably need to clone this commit [https://gitlab.com/izaya/CardJS/commit/028fe8a1c2cfc05402c44a969b4a740f3528d223](https://gitlab.com/izaya/CardJS/commit/028fe8a1c2cfc05402c44a969b4a740f3528d223)  